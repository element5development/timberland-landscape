<?php /*
The template for displaying the footer
*/ ?>
						
	<footer id="footer">
		<div id="copyright">©Copyright <?php echo date('Y'); ?> Timberland Landscape. All Rights Reserved. <a href="https://element5digital.com/" target="_blank" title="Element5 provides content and search marketing, web development and brand strategy." id="element5">Website created by Element5</a></div> 
		<a id="main-logo" href="/"><img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/footer-logo.png" alt="Timberland Landscape" /></a>
		<div style="clear: both"></div>
	</footer>
	
<!-- MASONRY GRID -->
  <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/JS/masonry.pkgd.min.js"></script>

</body>
</html>