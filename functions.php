<?php

//SETUP WORDPRESS & BACKEND
    // PULL PARENT THEME STYLES
        add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
        function theme_enqueue_styles() {
            wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
        }
    // REMOVE PARENT WIDGET AREAS
        function remove_parent_theme_widgets(){
            unregister_sidebar( 'sidebar-1' );
            unregister_sidebar( 'sidebar-2' );
            unregister_sidebar( 'sidebar-3' );
        }
        add_action( 'widgets_init', 'remove_parent_theme_widgets', 11 );
    // REMOVE PRARENT MENU AREAS
        add_action( 'after_setup_theme', 'remove_default_menu', 11 );
        function remove_default_menu(){
            unregister_nav_menu('primary');
            unregister_nav_menu('social');
        }
    // ALLOW SVG FILES IN WORDPRESS
        function cc_mime_types($mimes) {
          $mimes['svg'] = 'image/svg+xml';
          return $mimes;
        }
        add_filter('upload_mimes', 'cc_mime_types');
    // LIMIT NUMBER OF ARCHIEVES TO DISPLAY     // REMOVE IF DROPDOWN IS ENABLED //
        function my_limit_archives( $args ) {
            $args['limit'] = 10;
            return $args;
        }
        add_filter( 'widget_archives_args', 'my_limit_archives' );
        add_filter( 'widget_archives_dropdown_args', 'my_limit_archives' );
    // ADD JQUERY TO WORDPRESS
        if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
        function my_jquery_enqueue() {
           wp_deregister_script('jquery');
           wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js", false, null);
           wp_enqueue_script('jquery');
        }
    // ADD MENUS FOR NAVIGATION
        function register_menu () {
            register_nav_menu('main-menu', __('Header Menu'));
            register_nav_menu('mobile-menu', __('Mobile Menu'));
            register_nav_menu('error-menu', __('404 SiteMap'));
        }
        add_action('init', 'register_menu');
//SETUP WORDPRESS & BACKEND


//CUSTOMIZE CONTENT FOR CLIENT
    // SHORT CODES FOR CLIENT
        add_filter('widget_text', 'do_shortcode');
        function primary_button($atts, $content = null) {
            extract( shortcode_atts( array(
                'url' => '#'
            ), $atts ) );
            return '<a href="'.$url.'" class="primary-button">' . do_shortcode($content) . '<div class="primary-arrow"><img src="/wp-content/themes/timberland/img/icon-arrow-right.svg" /></div></a>';
        }
        add_shortcode('primary-btn', 'primary_button');
        function secondary_button($atts, $content = null) {
            extract( shortcode_atts( array(
                'url' => '#'
            ), $atts ) );
            return '<a href="'.$url.'" class="secondary-button">' . do_shortcode($content) . '<div class="secondary-arrow"><img src="/wp-content/themes/timberland/img/icon-arrow-red.svg" /></div></a>';
        }
        add_shortcode('secondary-btn', 'secondary_button');
        function legal_text($atts, $content = null) {
            extract( shortcode_atts( array( ), $atts ) );
            return '<p id="legal-text">' . do_shortcode($content) . '</p>';
        }
        add_shortcode('legal-text', 'legal_text');
    // ADD WIDGET AREAS FOR CONTENT
        if ( ! function_exists( 'custom_sidebar' ) ) {
            function custom_sidebar() {
                //TOP NAVIGATION WIDGET
                $args = array(
                    'id'            => 'top-nav',
                    'name'          => __( 'top-nav', 'twentythirteen' ),
                    'description'   => __( 'top-nav widget only on home page', 'twentythirteen' ),
                    'class'         => 'top-nav',
                    'before_title'  => '<h3 class="top-nav-title">',
                    'after_title'   => '</h3>',
                    'before_widget' => '<div id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</div>',
                );
                register_sidebar( $args );
                //Landscape Services WIDGET
                $args = array(
                    'id'            => 'landscape-services',
                    'name'          => __( 'landscape-services', 'twentythirteen' ),
                    'description'   => __( 'landscape-services widget links', 'twentythirteen' ),
                    'class'         => 'landscape-services',
                    'before_title'  => '<h3 class="landscape-service-title">',
                    'after_title'   => '</h3>',
                    'before_widget' => '<div id="%1$s" class="widget one-third %2$s">',
                    'after_widget'  => '</div>',
                );
                register_sidebar( $args );
                //Snow Services WIDGET
                $args = array(
                    'id'            => 'snow-services',
                    'name'          => __( 'snow-services', 'twentythirteen' ),
                    'description'   => __( 'snow-services widget links', 'twentythirteen' ),
                    'class'         => 'snow-services',
                    'before_title'  => '<h3 class="snow-services-title">',
                    'after_title'   => '</h3>',
                    'before_widget' => '<div id="%1$s" class="widget one-third %2$s">',
                    'after_widget'  => '</div>',
                );
                register_sidebar( $args );
            }
        add_action( 'widgets_init', 'custom_sidebar' );
        }
//CUSTOMIZE CONTENT FOR CLIENT

?>