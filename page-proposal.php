<?php /*
Template Name: Proposal
*/ ?>

<?php get_header(); ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>

<main class="full-width">

		<section id="page-header" class="full-width" style="background-image: url(<?php echo $src[0]; ?> );">
			<h1><?php echo get_the_title( $ID ); ?></h1>
		</section>
		<section id="page-intro" class="full-width">
			<?php the_field( 'page_intro' ) ?>
		</section>
		<div id="proposal-container" class="max-width">  
			<section id="proposal-form" class="full-width">
				<?php echo do_shortcode('[gravityform id="3" title="false" description="true"]'); ?>
			</section>
		</div>

</main>

<?php get_footer(); ?>