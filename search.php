<?php /*
The template for displaying search results pages
*/ ?>

<?php get_header(); ?>

<main class="full-width">

		<section id="page-header" class="full-width" style="background-image: url(/wp-content/themes/timberland/img/default-header.png);">
			<h1>Search Results</h1>
		</section>
		<section class="search-feed">
			<div class="max-width">
				<!-- Loop Start -->
				<?php if ( have_posts() ) : ?>
				<ul>
					<?php while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content-search', get_post_format() );
					endwhile; ?>
					<div style="clear: both"></div>
				</ul>
				<?php the_posts_pagination( array(
					'prev_text'          => __( 'Previous page', 'twentysixteen' ),
					'next_text'          => __( 'Next page', 'twentysixteen' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
				) );
				else : ?>
					<article>
						<h2>Nothing Found</h2>
					</article>
				<?php endif; ?>
				<!-- Loop End -->
			</div>
		</section>
		<section class="search-feed-form max-width">
			<h2>No Luck? Try Again.</h2>
			<?php get_search_form() ?>
		</section>
		
</main>

<?php get_footer(); ?>