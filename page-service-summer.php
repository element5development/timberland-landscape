<?php /*
Template Name: Service Summer
*/ ?>

<?php get_header(); ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>

<main class="full-width">

		<section id="page-header" class="full-width" style="background-image: url(<?php echo $src[0]; ?> );">
			<h1><?php echo get_the_title( $ID ); ?></h1>
		</section>
		<div id="service-container" class="full-width summer">  
			<div class="max-width">
				<section id="service-contents" class="one-half">
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; ?>
					<?php endif; ?>
				</section>
				<section id="service-extras" class="one-half">
					<?php the_field( 'service_slider' ) ?>
				</section>
				<div style="clear: both"></div>
			</div>
		</div>
		<section id="proposal-cta" class="full-width">
			<div class="one-third">
				<h3>Ready to work with us?</h3>
			</div>
			<div class="two-third">
				<div id="cta-container">
					<h2>Request a proposal</h2>
					<a href="/get-a-proposal/" class="secondary-button">Get A Quote<div class="secondary-arrow"><img src="/wp-content/themes/timberland/img/icon-arrow-red.svg"></div></a>
					<div style="clear: both"></div>
				</div>
			</div>
			<div style="clear: both"></div>
		</section>
</main>

<?php get_footer(); ?>