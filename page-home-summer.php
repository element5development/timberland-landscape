<?php /*
Template Name: Home Summer
*/ ?>

<?php get_header(); ?>

<main class="full-width">

		<section id="intro" class="full-width summer">
			<div id="home-cta-container">
				<a href="/landscape-management/">
					<div id="landscape" class="home-cta one-third" style="background-image: url(<?php the_field( 'call_out_one_image' ) ?>);">  
						<div class="contents"><?php the_field( 'call_out_one_content' ) ?></div>
						<div class="black-overlay"></div>
					</div>
				</a>
				<a href="/snow-ice-management/">
					<div id="snow-ice" class="home-cta one-third" style="background-image: url(<?php the_field( 'call_out_Two_image' ) ?>);">  
						<div class="contents"><?php the_field( 'call_out_two_content' ) ?></div>
						<div class="black-overlay"></div>
					</div>
				</a>
				<a href="/get-a-proposal/">
					<div id="request-proposal" class="home-cta one-third" style="background-image: url(<?php the_field( 'call_out_three_image' ) ?>);">  
						<div class="contents"><?php the_field( 'call_out_three_content' ) ?></div>
						<div class="black-overlay"></div>
					</div>
				</a>
			</div>
			<div class="videowrap">
				<div class="gradient">
					<div class="overlay">
						<video muted="" autoplay="" loop="" poster="<?php bloginfo('stylesheet_directory'); ?>/img/transparent.png" class="bgvid">
							<source src="<?php bloginfo('stylesheet_directory'); ?>/img/Timberland-Background.mp4" type="video/mp4">
							<source src="<?php bloginfo('stylesheet_directory'); ?>/img/Timberland-Background.webm" type="video/webm">
						</video>
					</div>
				</div>
			</div>
		</section>
		<section id="loge-slider" class="full-width">
			<h1>Metro-Detroitís Leading Companies Choose Timberland Landscape.</h1>
			<?php echo do_shortcode('[logoshowcase cat_id="13" dots="false" slides_column="5"]'); ?>
		</section>
		<section id="closing" class="full-width summer">
			<div class="max-width">
				<div id="landing-page" class="two-third" style="background-image: url(<?php the_field( 'landing_page_image' ) ?>);">  
					<a href="<?php the_field( 'landing_page_url' ) ?>"><div class="contents"><?php the_field( 'landing_page_content' ) ?></div></a>
					<div id="content-share" class="full-width">  
						<div id="link"><a href="<?php the_field( 'landing_page_url' ) ?>"><?php the_field( 'landing_page_cta_text' ) ?><img src="<?php bloginfo('stylesheet_directory'); ?>/img/icon-arrow-right.svg" /></a></div>
						<div style="clear: both"></div>
					</div>
					<div class="black-overlay"></div>
				</div>
				<div id="testimonals" class="one-third">  
					<img id="quotes" src="<?php bloginfo('stylesheet_directory'); ?>/img/quotes.svg" />
					<div class="contents"><?php echo do_shortcode('[testimonial id="106"]'); ?></div>
				</div>
				<div style="clear: both"></div>
			</div>
		</section>

</main>

<?php get_footer(); ?>