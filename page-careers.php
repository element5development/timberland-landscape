<?php /*
Template Name: Careers
*/ ?>

<?php get_header(); ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>

<main class="full-width">

		<section id="page-header" class="full-width" style="background-image: url(<?php echo $src[0]; ?> );">
			<h1><?php echo get_the_title( $ID ); ?></h1>
		</section>
		<div id="careers-container" class="max-width">  
			<section id="career-info" class="one-third">
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</section>
			<section id="career-feed" class="two-third">
				<h2>Apply Now</h2>
				<?php echo do_shortcode('[gravityform id="4" title="false" description="false"]'); ?>
			</section>
			<div style="clear: both"></div>
		</div>

</main>

<?php get_footer(); ?>