<?php /*
Template Name: About
*/ ?>

<?php get_header(); ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>

<main class="full-width">

		<section id="page-header" class="full-width" style="background-image: url(<?php echo $src[0]; ?> );">
			<h1><?php echo get_the_title( $ID ); ?></h1>
		</section>
		<section id="about-content" class="full-width">
			<div class="one-half">  
				<?php the_field( 'content_image' ) ?>
			</div>
			<div class="one-half">  
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
			<div style="clear: both"></div>
		</section>
		<section id="about-apply" class="max-width">
			<div id="apply-cta" class="full-width">
				<div class="contents">  
					Intrested in working with us?
					<a href="/contact/careers/" class="primary-button">Apply<div class="primary-arrow"><img src="/wp-content/themes/timberland/img/icon-arrow-right.svg"></div></a>
				</div>  
				<div style="clear: both"></div>
			</div>
		</section>

</main>

<?php get_footer(); ?>