/* Mobile Navigation Open and Close */

jQuery(document).ready(function() {
   $("#menu-icon").click(function(e) {
      if($("#menu-icon").hasClass("mobile-icon-close")) {
         $("#menu-icon").removeClass("mobile-icon-close");
         $("#mobile-nav-expand").removeClass('mobile-open');
      } else {
         $("#menu-icon").addClass("mobile-icon-close");
         $("#mobile-nav-expand").addClass('mobile-open');
      }
   });
});