/* Entrance Animations */

jQuery(document).ready(function() {
    jQuery('[DIV ID OR CLASS]').addClass("hidden").viewportChecker({
        classToAdd: 'visible animated [ANIMATION NAME]',
        offset: 100
       });
});