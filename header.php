<?php /*
The Header template for our theme
*/ ?>

<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
<!-- Favicon -->
	<link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/img/favicon.png" />
<!-- Main CSS -->
	<link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet" type="text/css" />
<!-- Default WP information -->
	<?php wp_head(); ?>
<!-- Entrance Animations -->
	<script src="<?php bloginfo('stylesheet_directory'); ?>/JS/jquery.viewportchecker.min.js"></script>
<!-- Mobile Site Media Query -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Smooth Scrolling -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/JS/smoothscroll.js"></script>
<!-- Mobile Nav -->
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/JS/mobilenav.js"></script>
<!-- Testimonals -->
		<?php if( is_page(12) || is_page(15) ) : ?>
			<script type="text/javascript">
				if (screen && screen.width > 750) {
					jQuery(document).ready(function(){
						$('#testimony-container .item:nth-child(1)').click(function() {
							$('#testimony-container .item:nth-child(5)').removeClass('active');
							$('#testimony-container .item:nth-child(4)').removeClass('active');
							$('#testimony-container .item:nth-child(3)').removeClass('active');
							$('#testimony-container .item:nth-child(2)').removeClass('active');
							$('#testimony-container .item:nth-child(1)').addClass('active');
						});
						$('#testimony-container .item:nth-child(2)').click(function() {
							$('#testimony-container .item:nth-child(5)').removeClass('active');
							$('#testimony-container .item:nth-child(4)').removeClass('active');
							$('#testimony-container .item:nth-child(3)').removeClass('active');
							$('#testimony-container .item:nth-child(1)').removeClass('active');
							$('#testimony-container .item:nth-child(2)').addClass('active');
							$('#testimony-container .item:nth-child(1) .up_arrow').css({"opacity" : "0", "z-index" : "0"});
							$('#testimony-container .item:nth-child(1) .excerpt').css({"opacity" : "0", "z-index" : "0"});
							$('#testimony-container .item:nth-child(1) .title').css({"opacity" : "0", "z-index" : "0"});
						});
						$('#testimony-container .item:nth-child(3)').click(function() {
							$('#testimony-container .item:nth-child(5)').removeClass('active');
							$('#testimony-container .item:nth-child(4)').removeClass('active');
							$('#testimony-container .item:nth-child(2)').removeClass('active');
							$('#testimony-container .item:nth-child(1)').removeClass('active');
							$('#testimony-container .item:nth-child(3)').addClass('active');
							$('#testimony-container .item:nth-child(1) .up_arrow').css({"opacity" : "0", "z-index" : "0"});
							$('#testimony-container .item:nth-child(1) .excerpt').css({"opacity" : "0", "z-index" : "0"});
							$('#testimony-container .item:nth-child(1) .title').css({"opacity" : "0", "z-index" : "0"});
						});
						$('#testimony-container .item:nth-child(4)').click(function() {
							$('#testimony-container .item:nth-child(5)').removeClass('active');
							$('#testimony-container .item:nth-child(3)').removeClass('active');
							$('#testimony-container .item:nth-child(2)').removeClass('active');
							$('#testimony-container .item:nth-child(1)').removeClass('active');
							$('#testimony-container .item:nth-child(4)').addClass('active');
							$('#testimony-container .item:nth-child(1) .up_arrow').css({"opacity" : "0", "z-index" : "0"});
							$('#testimony-container .item:nth-child(1) .excerpt').css({"opacity" : "0", "z-index" : "0"});
							$('#testimony-container .item:nth-child(1) .title').css({"opacity" : "0", "z-index" : "0"});
						});
						$('#testimony-container .item:nth-child(5) .thumb').click(function() {
							$('#testimony-container .item:nth-child(4)').removeClass('active');
							$('#testimony-container .item:nth-child(3)').removeClass('active');
							$('#testimony-container .item:nth-child(2)').removeClass('active');
							$('#testimony-container .item:nth-child(1)').removeClass('active');
							$('#testimony-container .item:nth-child(5)').addClass('active');
							$('#testimony-container .item:nth-child(1) .up_arrow').css({"opacity" : "0", "z-index" : "0"});
							$('#testimony-container .item:nth-child(1) .excerpt').css({"opacity" : "0", "z-index" : "0"});
							$('#testimony-container .item:nth-child(1) .title').css({"opacity" : "0", "z-index" : "0"});
						});
					})
				}
			</script>
		<?php endif; ?>
<!-- Snow Fall -->
		<?php if( is_front_page() ) { ?>
			<!-- Snow Falling Effect -->
			<script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/JS/snowfall.js"></script>
			<!-- Share Buttons -->
		<?php } ?>
</head>

<body <?php body_class(); ?>>
	<div id="top-nav" class="full-width">
		<div id="top-nav-widgets" class="widget-area-container">
			<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('top-nav')) : else : ?>
				<p><strong>Widget Ready</strong></p>  
			<?php endif; ?>  
			<div style="clear: both"></div>
		</div>
		<div style="clear: both"></div>
	</div>
	<header id="main-nav" class="full-width">
		<a id="main-logo" href="/"><img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/menu-logo.png" alt="Timberland Landscape" /></a>
		<nav>
			<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
			<div style="clear: both"></div>
		</nav> 
		<a href="tel:+12482768800" id="phone-link">(248) 276-8800</a>
	</header>
	<header id="mobile-nav" class="full-width">
		<div class="full-width">  
			<a href="tel:+12482768800" id="phone-link"></a>
			<a href="/"><img id="nav-logo" src="<?php bloginfo('stylesheet_directory'); ?>/img/menu-logo.png" alt="Timberland Landscape" /></a>
			<div id="menu-icon"></div>
			<div style="clear: both"></div>
		</div>
		<div id="mobile-nav-expand" class="full-width"> <!--Open Menu Contents -->
			<nav>
				<?php wp_nav_menu( array( 'theme_location' => 'mobile-menu' ) ); ?>
				<div style="clear: both"></div>
			</nav> 
			<div style="clear: both"></div>
		</div>
	</header>