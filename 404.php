<?php /*
The template for displaying 404 pages (broken links)
*/ ?>

<?php get_header(); ?>

<main class="full-width">

	<section id="page-header" class="full-width" style="background-image: url(/wp-content/themes/timberland/img/default-header.png);">
		<h1>Sorry, Can't Seem to Find the Page</h1>
	</section>
	<section class="error-404 not-found">
			<div class="error-search-form max-width"><?php get_search_form(); ?></div>
			<div class="error-sitemap-menu max-width">
				<?php wp_nav_menu( array( 'theme_location' => 'error-menu' ) ); ?> 
			</div>
	</section>
	<section id="loge-slider" class="full-width">
		<h3>Metro-Detroit’s Best Companies Choose Timberland</h3>
		<?php echo do_shortcode('[logoshowcase cat_id="13" dots="false" slides_column="5"]'); ?>
	</section>
</main>

<?php get_footer(); ?>
