<?php /*
Template Name: Snow & Ice Management
*/ ?>

<?php get_header(); ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>

<main class="full-width">

		<section id="page-header" class="full-width" style="background-image: url(<?php echo $src[0]; ?> );">
			<h1><?php echo get_the_title( $ID ); ?></h1>
		</section>
		<div id="default-page-temp" class="full-width">  
			<div class="max-width">
				<section id="service-parent-page">
					<div class="widget-area-container">
						<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('snow-services')) : else : ?>
							<p><strong>Widget Ready</strong></p>  
						<?php endif; ?>  
						<div style="clear: both"></div>
					</div>
				</section>
			</div>
		</div>
		<section id="proposal-cta" class="full-width">
			<div class="one-third">
				<h3>Ready to work with us?</h3>
			</div>
			<div class="two-third">
				<div id="cta-container">
					<h2>Request a proposal</h2>
					<a href="/get-a-proposal/" class="secondary-button">Get A Quote<div class="secondary-arrow"><img src="/wp-content/themes/timberland/img/icon-arrow-red.svg"></div></a>
					<div style="clear: both"></div>
				</div>
			</div>
			<div style="clear: both"></div>
		</section>
</main>

<?php get_footer(); ?>