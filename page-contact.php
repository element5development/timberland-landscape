<?php /*
Template Name: Contact
*/ ?>

<?php get_header(); ?>

<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ); ?>

<main class="full-width">

		<section id="page-header" class="full-width" style="background-image: url(<?php echo $src[0]; ?> );">
			<h1><?php echo get_the_title( $ID ); ?></h1>
		</section>
		<div id="contact-container" class="max-width">  
			<section id="contact-form" class="one-half">
				<h2>Have A Question</h2>
				<h3>Send us a message</h3>
				<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
			</section>
			<section id="contact-contents" class="one-half">
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<a href="tel:1-248-276-8800" class="phone">OFFICE: +1 (248) 276-8800</a><br/>
				<a htef="" class="fax">FAX: +1 (248) 276-2529</a>
				<div class="proposal-cta">  
					<h2>WANT TO GET<br/>A PROPOSAL?</h2>
					<div id="link"><a href="/get-a-proposal/">Click here to get a proposal <img src="http://e5timberland.wpengine.com/wp-content/themes/timberland/img/icon-arrow-right.svg"></a></div>
					<img id="construction-car" src="<?php bloginfo('stylesheet_directory'); ?>/img/contat-graphic.png" />
				</div>
			</section>
			<div style="clear: both"></div>
		</div>
		<section id="about-apply" class="max-width">
			<div id="apply-cta" class="full-width">
				<div class="contents">  
					Interested in working with us?
					<a href="/contact/careers/" class="primary-button">Apply<div class="primary-arrow"><img src="/wp-content/themes/timberland/img/icon-arrow-right.svg"></div></a>
				</div>  
				<div style="clear: both"></div>
			</div>
		</section>

</main>

<?php get_footer(); ?>