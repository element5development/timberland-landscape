<?php /*
The template part for displaying main blog feed
*/ ?>

<article id="post-<?php the_ID(); ?>" class="post-feed">

	<h2 class="entry-header"><?php the_title( sprintf( '<a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a>' ); ?></h2>  <!-- Display Post Title & Link to the Post -->
	
	<a href="<?php the_permalink(); ?>">Post Link</a> <!-- Provides Link to the Post -->

	<div class="post-feed-image"><?php THE_post_thumbnail(); ?></div> <!-- Display the Post Thumbnail -->

	<div class="post-feed-date"><?php the_time('F j, Y') ?></div> <!-- Display the Post Date -->

	<div class="post-feed-author"><?php the_author(); ?> </div> <!-- Display the Post Author -->

	<div class="post-feed-cat"><?php the_category(', ') ?></div> <!-- Display the Post Category -->

	<div class="post-feed-content"><?php the_content(); ?></div> <!-- Display the Post Content -->

	<div class="post-feed-excerpt"><?php tHE_excerpt(); ?></div> <!-- Pull Post Excerpt -->

</article>
