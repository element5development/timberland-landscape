<?php /*
The template part for displaying single post
*/ ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<h1 class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</h1>  <!-- Display Post Title & Link to the Post -->
	
	<a href="<?php the_permalink(); ?>">Post Link</a> <!-- Provides Link to the Post -->

	<div class="post-feed-image"><?php twentysixteen_post_thumbnail(); ?></div> <!-- Display the Post Thumbnail -->

	<div class="post-feed-date"><?php the_time('F j, Y') ?></div> <!-- Display the Post Date -->

	<div class="post-feed-author"><?php the_author(); ?> </div> <!-- Display the Post Author -->

	<div class="post-feed-cat"><?php the_category(', ') ?></div> <!-- Display the Post Category -->

	<div class="post-feed-content"><?php the_content(); ?></div> <!-- Display the Post Content -->

	<div class="post-feed-excerpt"><?php twentysixteen_excerpt(); ?></div> <!-- Pull Post Excerpt -->

</article><!-- #post-## -->
