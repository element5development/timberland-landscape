<?php /*
The template part for displaying results in search pages
*/ ?>

<?php if( $post->post_type == 'page' ) { ?>

<li id="post-<?php the_ID(); ?>" class="post-feed search-feed one-third">
	
	<h2 class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</h2>  <!-- Display Post Title & Link to the Post -->

	<div class="post-feed-image">
		<?php
			if ( has_post_thumbnail() )
				the_post_thumbnail();
			else
				echo '<img src="' . trailingslashit( get_stylesheet_directory_uri() ) . 'images/default-thumbnail.png' . '" alt="" />';
		?>
	</div> <!-- Display the Post Thumbnail -->

	<div class="post-feed-excerpt"><?php the_excerpt(); ?></div> <!-- Pull Post Excerpt -->
</li>

<?php } else {} ?>